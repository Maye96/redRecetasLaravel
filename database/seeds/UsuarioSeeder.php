<?php

use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UsuarioSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::create([
            'name' => 'Maye',
            'email' => 'maye@correo.com',
            'password' => Hash::make('123456789'),
            'url' => 'http://maye.com',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ]);
     

        $user2 = User::create([
            'name' => 'Nazareth',
            'email' => 'nazareth@correo.com',
            'password' => Hash::make('123456789'),
            'url' => 'http://nazareth.com',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ]);
       
    }
}
