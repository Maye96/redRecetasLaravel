<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Perfil extends Model
{
    //
    protected $fillable = [
        'user_id', 'biografia', 'imagen', 'user_id',
    ];

    // relacion 1:1  de perfil con usuario 
    public function usuario()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
