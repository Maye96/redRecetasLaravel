<?php

namespace App\Http\Controllers;

use App\Receta;
use App\CategoriaReceta;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use PhpParser\Node\Stmt\Return_;

class InicioController extends Controller
{
    //
    public function index()
    {
        // Mostrar las recetas por cantidad de votos
        // $votadas = Receta::has('likes', '>', 1)->get();

        // withCount permite crear una columna momentanea con la cantidad de likes que posee cada una, toma orderBy de forma desc
        $votadas = Receta::withCount('likes')->orderBy('likes_count', 'desc')->take(3)->get();

        // Consultar las ultima recetas (mas nuevas)
        // latest() es equivalente a orderBy('created_at', 'DESC')
        // Los primeros: oldest
        $nuevas = Receta::latest()->take(10)->get();

        // Obtener todas las cartegorias
        $categorias = CategoriaReceta::all();

        // Agrupar las recetas por categorias
        $recetas = [];

        foreach($categorias as $categoria) 
        {
            // Str::slug(cadena): Helper que sustituye espacios con - y todas las letras en minuscula
            $recetas[Str::slug($categoria->nombre)][] = Receta::where('categoria_id',$categoria->id )->take(3)->get();
        }

        return view('inicio.index', compact('nuevas', 'recetas', 'votadas'));
    }
}
