<?php

namespace App\Http\Controllers;

use App\Perfil;
use App\Receta;
use App\User;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;

class PerfilController extends Controller
{

    public function __construct()
    {
        // El usuario debe estar autentificado
        $this->middleware('auth', ['except' => 'show']);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Perfil  $perfil
     * @return \Illuminate\Http\Response
     */
    public function show(Perfil $perfil)
    {
        // Obtener las recetas con paginacion
        $recetas  = Receta::where('user_id', $perfil->user_id)->paginate(4);
        return view('perfiles.show', compact('perfil', 'recetas'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Perfil  $perfil
     * @return \Illuminate\Http\Response
     */
    public function edit(Perfil $perfil)
    {
        // Ejecutar Policy, para que un usuario no pueda ver el editar perfil de otro
        $this->authorize('view', $perfil);
        return view('perfiles.edit', compact('perfil'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Perfil  $perfil
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Perfil $perfil)
    {
        // Ejecutar el policy
        $this->authorize('update', $perfil);
       
        // Validar
        $data = request()->validate([
            'nombre'    => 'required',
            'url'    => 'required',
            'biografia' => 'required',
        ]);

        // Si el usuario sube una imagen
        if($request['imagen']) {
            // Obtner la ruta de la imagen
            $ruta_imagen = $request['imagen']->store('upload-perfiles', 'public');
        
            // Resize de la imagen (Toma la imagen, la recorta y la guarda)
            $img = Image::make(public_path("storage/{$ruta_imagen}"))->fit(600,600);
            $img->save();

            // Crear un arreglo de la imagen
            $array_imagen = ['imagen' => $ruta_imagen];
        }
       
        // Asignar nombre y  URL
        auth()->user()->url = $data['url'];
        auth()->user()->name = $data['nombre'];
        auth()->user()->save();

        // Eliminar URL y nombre de $data
        unset($data['url']);
        unset($data['nombre']);

        // Guardar
        // Asignar Biografia e imagen
        // array_merge mezcla arreglos
        auth()->user()->perfil()->update( array_merge(
            $data,
            $array_imagen ?? []
        ));
        
        // redireccionar
        return redirect()->action('RecetaController@index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Perfil  $perfil
     * @return \Illuminate\Http\Response
     */
    public function destroy(Perfil $perfil)
    {
        //
    }
}
