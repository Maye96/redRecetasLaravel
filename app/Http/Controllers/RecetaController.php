<?php

namespace App\Http\Controllers;

use App\Receta;
use App\CategoriaReceta;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Intervention\Image\Facades\Image;

class RecetaController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth', ['except' => ['show', 'search']]);
        ///Si se desea pasar varios metodos usar ['except' => ['show', 'create]]

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // auth()->user()->recetas->dd();

        // $recetas = auth()->user()->recetas;
        // Recetas con paginacion
        $usuario = auth()->user();
        $recetas = Receta::where('user_id', $usuario->id)->paginate(2);
        /// Pasar al usuario
        return view('recetas.index', compact('recetas', 'usuario'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $categorias = CategoriaReceta::all(['id', 'nombre']);

        return view('recetas.create')->with('categorias', $categorias);;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $data = $request->validate([
            'titulo'    => 'required|min:6',
            'categoria' => 'required',
            'preparacion' => 'required',
            'ingredientes' => 'required',
            'imagen' => 'required|image',
        ]);

        ///Obtner la ruta de la imagen
        $ruta_imagen = $request['imagen']->store('upload-recetas', 'public');
        
        // Resize de la imagen (Toma la imagen, la recorta y la guarda)
        $img = Image::make(public_path("storage/{$ruta_imagen}"))->fit(1000,550);
        $img->save();

        /// Almacenar en la DB (sin modelo)
        /*** 
        DB::table('recetas')->insert([
            'titulo' => $data['titulo'],
            'preparacion'  => $data['preparacion'],
            'ingredientes' => $data['ingredientes'],
            'imagen'=>  $ruta_imagen,
            'user_id'=> Auth::user()->id,
            'categoria_id'=> $data['categoria']
        ]);
        ***/

        // Almacenar en la DB (con modelo)
        auth()->user()->recetas()->create([
            'titulo' => $data['titulo'],
            'preparacion'  => $data['preparacion'],
            'ingredientes' => $data['ingredientes'],
            'imagen'=>  $ruta_imagen,
            'categoria_id'=> $data['categoria']
        ]);


        // Redireccionar
        return redirect()->action('RecetaController@index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Receta  $receta
     * @return \Illuminate\Http\Response
     */
    public function show(Receta $receta)
    {
        // Algunos metodos para obtener receta
        /***
        $receta = Receta::find($receta);
        $receta = Receta::findOrFail($receta);
        ***/
        
        // Obtener si el usuario actual le gusta la receta y esta autentificado
        $like = (auth()->user() ? auth()->user()->meGusta->contains($receta->id )  :false);
        // Pasar la cantidad de likes
        $likes = $receta->likes->count();

        return view('recetas.show', compact('receta', 'like', 'likes'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Receta  $receta
     * @return \Illuminate\Http\Response
     */
    public function edit(Receta $receta)
    {
        /// Ejecutar el policy
        $this->authorize('view', $receta);

        $categorias = CategoriaReceta::all(['id', 'nombre']);

        return view('recetas.edit', compact('categorias','receta'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Receta  $receta
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Receta $receta)
    {
        /// Revisar el policy
        $this->authorize('update', $receta);
        
        $data = $request->validate([
            'titulo'    => 'required|min:6',
            'categoria' => 'required',
            'preparacion' => 'required',
            'ingredientes' => 'required',
        ]);

        // Asignar los valores
        $receta->titulo = $data['titulo'];
        $receta->preparacion = $data['preparacion'];
        $receta->ingredientes = $data['ingredientes'];
        $receta->categoria_id = $data['categoria'];

        // Si el usuario sube una nueva imagen
        if(request('imagen')) 
        {
            ///Obtner la ruta de la imagen
            $ruta_imagen = $request['imagen']->store('upload-recetas', 'public');
            
            // Resize de la imagen (Toma la imagen, la recorta y la guarda)
            $img = Image::make(public_path("storage/{$ruta_imagen}"))->fit(1000,550);
            $img->save();

            // Asignar al objeto
            $receta->imagen = $ruta_imagen;
        }

        $receta->save();

        // Redireccionar
        return  redirect()->action('RecetaController@index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Receta  $receta
     * @return \Illuminate\Http\Response
     */
    public function destroy(Receta $receta)
    {
        // Ejecutar el policy
        $this->authorize('delete', $receta);

        // Eliminar la receta
        $receta->delete();

        return redirect()->action('RecetaController@index');
    }

    public function search(Request $request)
    {
        // $busqueda = $request['buscar'];
        $busqueda = $request->get('buscar');

        $recetas = Receta::where('titulo', 'like', '%'.$busqueda.'%')->paginate(5);
        // Agregar algo al query string, es para que se mantenga el vlor del buscar
        $recetas->appends(['buscar'=> $busqueda]);

        return view('busquedas.show', compact('recetas', 'busqueda'));
    }
}
