<?php

namespace App\Http\Controllers;

use App\Receta;
use Illuminate\Http\Request;

class LikesController extends Controller
{
   public function __construct()
   {
       // Se requiere estar autentificado para acceder al metodo update (meGusta)
       $this->middleware('auth');
   }

    public function update(Request $request, Receta $receta)
    {
         // Almacena los usuarios de una receta
        /*
         * toggle:  Funcion que si esta y alguien presiona colocara el me gusta, 
         * sino esta y alguien presiona colocara el me gusta 
         * tambien se usa: "En me gusta no me gusta
         **/
        return auth()->user()->meGusta()->toggle($receta);
       
    }
}


