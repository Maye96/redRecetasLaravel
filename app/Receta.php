<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Receta extends Model
{
    protected $fillable = [
        'titulo', 'ingredientes', 'preparacion', 'imagen', 'user_id', 'categoria_id',
    ];

    // Obtiene la categoria de la receta via FK
    public function categoria()
    {
        return $this->belongsTo(CategoriaReceta::class);
    }

    // Obtiene la inf. del usuario via FK
    public function autor()
    {
                                            /*Clave Foranea*/
        return $this->belongsTo(User::class, 'user_id');
    }

    // Likes que ha recibido una receta
    public function likes()
    {
                                                /* Ref. a la tabla pivote */
        return $this->belongsToMany(User::class, 'likes_receta');
    }
}
