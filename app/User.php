<?php

namespace App;

use GuzzleHttp\Promise\Create;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'url',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * 
     * Evento que se ejecuta cuando un usuario es creado
     *  obj: Crear un perfil luego de que se registra un usuario
     * 
     */

    protected static function boot()
    {
        /// llama al metodo boot de la clase padre
        parent::boot();

        /// Asignar perfil una vez se haya creado un usuario nuevo
        static::created(function($user){
            $user->perfil()->create();
        });

    }
    /*** Relacion de 1:n de Usuario a Recetas ***/
    public function recetas()
    {
        return $this->hasMany(Receta::class);
    }

    /*** Un usuario va a tener un perfil ***/
    public function perfil()
    {
        return $this->hasOne(Perfil::class);
    }

    /*** Recetas que el usuario ha dado me gusta ***/
    public function meGusta()
    {
                                                   /* Ref. a la tabla pivote */
        return $this->belongsToMany(Receta::class, 'likes_receta');
    }
}
